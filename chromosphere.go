// chromosphere
package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"syscall"
	"time"
	"unsafe"
)

func main() {
	rs := red()
	blue := blue()

	red := ""
	for _, v := range rs {
		red += strconv.Itoa(v) + " "
	}
	fmt.Println("生产的选号:")
	fmt.Println("红球: " + red + "  篮球: " + strconv.Itoa(blue))
	content := "红球: " + red + "  篮球: " + strconv.Itoa(blue)
	defer syscall.FreeLibrary(kernel32)
	defer syscall.FreeLibrary(user32)

	fmt.Printf("Retern: %d\n", MessageBox("选号", content, MB_YESNOCANCEL))
}

func red() *[6]int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	rs := new([6]int)
	data := make(map[int]int)
	for i := 0; i < 6; i++ {
		//fmt.Printf("%d,", r.Intn(34))
		t := r.Intn(34)
		if t != 0 && data[t] != 1 {
			rs[i] = t
			data[t] = 1
		} else {
			i--
		}
	}
	rs = sort(rs)
	return rs
}

func blue() int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	t := r.Intn(17)
	if t == 0 {
		t = blue()
	}
	return t
}

func sort(data *[6]int) *[6]int {
	for i := 0; i < 6; i++ { /*外循环：控制比较趟数*/
		for j := 5; j > i; j-- { /*内循环：进行每趟比较*/
			if data[j] < data[j-1] { /*如果data[j]大于data[j-1],交换两者的位置*/
				temp := data[j]
				data[j] = data[j-1]
				data[j-1] = temp
			}
		}
	}
	return data
}
func abort(funcname string, err int) {
	panic(funcname + " failed: " + syscall.Errno(err).Error())
}

var (
	kernel32, _        = syscall.LoadLibrary("kernel32.dll")
	getModuleHandle, _ = syscall.GetProcAddress(kernel32, "GetModuleHandleW")

	user32, _     = syscall.LoadLibrary("user32.dll")
	messageBox, _ = syscall.GetProcAddress(user32, "MessageBoxW")
)

const (
	MB_OK                = 0x00000000
	MB_OKCANCEL          = 0x00000001
	MB_ABORTRETRYIGNORE  = 0x00000002
	MB_YESNOCANCEL       = 0x00000003
	MB_YESNO             = 0x00000004
	MB_RETRYCANCEL       = 0x00000005
	MB_CANCELTRYCONTINUE = 0x00000006
	MB_ICONHAND          = 0x00000010
	MB_ICONQUESTION      = 0x00000020
	MB_ICONEXCLAMATION   = 0x00000030
	MB_ICONASTERISK      = 0x00000040
	MB_USERICON          = 0x00000080
	MB_ICONWARNING       = MB_ICONEXCLAMATION
	MB_ICONERROR         = MB_ICONHAND
	MB_ICONINFORMATION   = MB_ICONASTERISK
	MB_ICONSTOP          = MB_ICONHAND

	MB_DEFBUTTON1 = 0x00000000
	MB_DEFBUTTON2 = 0x00000100
	MB_DEFBUTTON3 = 0x00000200
	MB_DEFBUTTON4 = 0x00000300
)

func MessageBox(caption, text string, style uintptr) (result int) {
	// var hwnd HWND
	ret, _, callErr := syscall.Syscall6(uintptr(messageBox), 4,
		0, // HWND
		uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(text))),    // Text
		uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(caption))), // Caption
		style,                                                      // type
		0,
		0)
	if callErr != 0 {
		abort("Call MessageBox", int(callErr))
	}
	result = int(ret)
	return
}
